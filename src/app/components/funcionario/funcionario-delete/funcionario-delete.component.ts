import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Funcionario } from 'src/app/models/funcionario.model';
import { FuncionarioService } from 'src/app/services/funcionario.service';

@Component({
  selector: 'app-funcionario-delete',
  templateUrl: './funcionario-delete.component.html',
  styleUrls: ['./funcionario-delete.component.css']
})
export class FuncionarioDeleteComponent implements OnInit {

  funcionario: Funcionario;

  constructor(
    private funcionarioService: FuncionarioService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.funcionarioService.readById(+id).subscribe(funcionario => {
        this.funcionario = funcionario;
      })
    }
  }

  deletarFuncionario(): void {
    if (this.funcionario.id) {
      this.funcionarioService.delete(this.funcionario.id).subscribe(() => {
        this.funcionarioService.showMessage('Dados deletados!');
        this.router.navigate(['/funcionarios']);
      });
    }
  }

  cancel(): void {
    this.router.navigate(['/funcionarios']);
  }
}
