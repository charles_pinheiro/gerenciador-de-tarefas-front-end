import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Funcionario } from 'src/app/models/funcionario.model';
import { Tarefa } from 'src/app/models/tarefa.model';
import { FuncionarioService } from 'src/app/services/funcionario.service';

@Component({
  selector: 'app-tarefa-delete',
  templateUrl: './tarefa-delete.component.html',
  styleUrls: ['./tarefa-delete.component.css']
})
export class TarefaDeleteComponent implements OnInit {

  funcionario: Funcionario;
  tarefa: Tarefa;

  constructor(
    private funcionarioService: FuncionarioService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const f_id = this.route.snapshot.paramMap.get('f_id');
    if (f_id) {
      this.funcionarioService.readById(+f_id).subscribe(funcionario => {
        this.funcionario = funcionario;
      })
    }
    console.log(this.funcionario.tarefas);
    const t_id = this.route.snapshot.paramMap.get('id');
    if (this.funcionario.tarefas && t_id) {
      let tarefa_atual = this.funcionario.tarefas[+t_id];
    }
  }

  cancel(): void {
    const id = this.route.snapshot.paramMap.get('f_id');
    this.router.navigate([`/funcionarios/${id}`]);
  }
}
