export interface Tarefa {
    id: number;
    nome: string;
    investimento: number;
    inicio: string;
    termino: string;
}