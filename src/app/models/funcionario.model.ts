import { Tarefa } from "./tarefa.model";

export interface Funcionario {
    id?: number;
    nome: string;
    cargo: string;
    tarefas?: Array<Tarefa>;
}