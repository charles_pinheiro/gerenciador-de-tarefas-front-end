import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { FuncionarioCrudComponent } from './views/funcionario-crud/funcionario-crud.component';
import { FuncionarioCreateComponent } from './components/funcionario/funcionario-create/funcionario-create.component';
import { FuncionarioUpdateComponent } from './components/funcionario/funcionario-update/funcionario-update.component';
import { FuncionarioDeleteComponent } from './components/funcionario/funcionario-delete/funcionario-delete.component';
import { FuncionarioReadDetailComponent } from './components/funcionario/funcionario-read-detail/funcionario-read-detail.component';
import { TarefaCreateComponent } from './components/tarefa/tarefa-create/tarefa-create.component';
import { TarefaDeleteComponent } from './components/tarefa/tarefa-delete/tarefa-delete.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'funcionarios',
    component: FuncionarioCrudComponent,
  },
  {
    path: 'funcionarios/registrar',
    component: FuncionarioCreateComponent,
  },
  {
    path: 'funcionarios/editar/:id',
    component: FuncionarioUpdateComponent,
  },
  {
    path: 'funcionarios/excluir/:id',
    component: FuncionarioDeleteComponent,
  },
  {
    path: 'funcionarios/:id',
    component: FuncionarioReadDetailComponent,
  },
  {
    path: 'funcionarios/:id/tarefas/criar',
    component: TarefaCreateComponent,
  },
  {
    path: 'funcionarios/:f_id/tarefas/excluir/:id',
    component: TarefaDeleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
