import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tarefa } from '../models/tarefa.model';

@Injectable({
  providedIn: 'root'
})
export class TarefaService {

  baseURL = 'http://localhost:3000/tarefas';

  constructor(
    private http: HttpClient
  ) { }

  readById(id: number): Observable<Tarefa> {
    const url = `${this.baseURL}/${id}`;
    return this.http.get<Tarefa>(url);
  }
}
